const express = require('express');
const loginController = require('../controllers/loginController');
// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', loginController.view_login);

router.post('/submit', (req, res) => {
  if (req.body.data) {
    res.send({
      data: 'hi',
      error: null,
    });
  } else {
    res.send({
      data: null,
      error: 'hi',
    });
  }
});

module.exports = router;
