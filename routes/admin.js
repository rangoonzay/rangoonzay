const express = require('express');
const userController = require('../controllers/admin/userController');
// eslint-disable-next-line new-cap
const router = express.Router();
const auth = require('../middleware/auth');

// router.post('/brands/create', auth, brandController.create_brand);

/** *
 * User Routes
 */
/**
 * Public routes
 */
// router.get('/docs', brandController.get_docs);
router.post('/create', userController.user_create);
router.post('/login', userController.user_login);

/**
 * Private routes
 */

router.get('/me', auth, userController.user_profile);
router.put('/me', auth, userController.user_update);
router.delete('/me', auth, userController.user_remove);
router.put('/me/password', auth, userController.user_password_change);

module.exports = router;
