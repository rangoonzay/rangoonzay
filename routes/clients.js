const express = require('express');
const clientController = require('../controllers/clients/clientController');
// eslint-disable-next-line new-cap
const router = express.Router();
const auth = require('../middleware/auth');

router.get('/', auth, clientController.home_page);
router.get('/i/:itemId', auth, clientController.product_detail);

module.exports = router;
