const express = require('express');
const multer = require('multer');

const BaseController = require('../controllers/shops/BaseController');
const shopController = require('../controllers/shops/shopController');
const pdfController = require('../controllers/shops/pdfController');
const ProductController = require('../controllers/shops/ProductController');
const ShippingController = require('../controllers/shops/shippingController');
const fdlController = require('../controllers/shops/fdlController');
const Contact = require('../models/contact');
// eslint-disable-next-line new-cap
const router = express.Router();
const upload = multer({dest: 'uploads/'});
const auth = require('../middleware/auth');

/**
 * View routes
 */
router.get('/', auth, shopController.shop_home);
router.get('/contacts', auth, shopController.view_contact);
router.get('/products', auth, shopController.view_product);
router.get('/vouchers', auth, shopController.view_voucher);
router.get('/shippings', auth, shopController.view_shipping);
router.get('/profile', auth, shopController.view_profile);
router.get('/account', auth, shopController.view_account);

/**
 * Contacts routes
 */
const contactCtrl = new BaseController(Contact);
router.get('/api/contact', contactCtrl.list);
router.post('/api/contact', contactCtrl.post);
router.get('/api/contact/:id', contactCtrl.get);
router.put('/api/contact/:id', contactCtrl.put);
router.delete('/api/contact/:id', contactCtrl.delete);

/**
 * Products routes
 */
const Product = require('../models/product');
const productCtrl = new ProductController(Product);
const awscb = upload.array('productAlbum', 10);
router.get('/api/product', productCtrl.list);
router.post('/api/product', awscb, productCtrl.post);
router.get('/api/product/:id', productCtrl.get);
router.put('/api/product/:id', awscb, productCtrl.put);
router.delete('/api/product/:id', productCtrl.delete);

/**
 * Shippings routes
 */
const Shipping = require('../models/shipping');
const shippingCtrl = new ShippingController(Shipping);
router.get('/api/shipping', shippingCtrl.list);
router.post('/api/shipping', shippingCtrl.post);
router.get('/api/shipping/:id', shippingCtrl.get);
router.put('/api/shipping/:id', shippingCtrl.put);
router.delete('/api/shipping/:id', shippingCtrl.delete);

/**
 * Vouchers routes
 */
const Voucher = require('../models/voucher');
const VoucherController = require('../controllers/shops/VoucherController');
const voucherCtrl = new VoucherController(Voucher);
router.get('/api/voucher', voucherCtrl.list);
router.post('/api/voucher', voucherCtrl.post);
router.get('/api/voucher/:id', voucherCtrl.get);
router.put('/api/voucher/:id', voucherCtrl.put);
router.delete('/api/voucher/:id', voucherCtrl.delete);

/**
 * VoucherItem routes
 */
const VoucherItem = require('../models/voucherItem');
const VoucherItemController =
  require('../controllers/shops/VoucherItemController');
const voucherItemCtrl = new VoucherItemController(VoucherItem);
router.post('/api/voucheritem/b', voucherItemCtrl.bulkPost);

/**
 * FlexDataList data end points
 */
router.get('/fdl/products', fdlController.flexdatalist_product);

/**
 * Generate PDF of shipping label
 */
router.post('/shipping-label', pdfController.createShippingLabel);

module.exports = router;
