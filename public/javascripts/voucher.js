$(function() {
  const userId = JSON.parse(localStorage.getItem('user'))._id;

  // editBtn and its event
  const editBtn = {
    text: 'Edit',
    className: 'btn btn-outline-dark btn-sm disabled',
    attr: {id: 'voucher-editButton'},
    action: function(e, dt, node, config) {
      // populate form with table row value
      const cellIndex = dt.cell('.selected', 0).index();
      const data = dt.row(cellIndex.row).data();
      console.log(data);

      for (const product of data.products) {
        product.productName = product.productId.name;
      }

      // TODO: hacky. will change later
      data.payment.paid = data.paid;
      data.buyerId.country = 'Myanmar';

      localStorage.setItem('voucher-to-checkout', JSON.stringify(data));

      // redirect to checkout page
      window.location.href = '/shops';
    },
  };

  const detailRows = [];
  const voucherTable = getDataTable(
      [
        {
          width: '18%',
          targets: [1],
        },
        {
          targets: 4,
          render: function(data) {
            return data.toLocaleString();
          },
        },
        {
          targets: 5,
          render: function(data) {
            return data.toLocaleString();
          },
        },
        {
          targets: 6,
          render: function(data) {
            return moment(data).format('YYYY-MM-DD hh:mm A');
          },
        },
      ],
      `/shops/api/voucher?sellerId=${userId}`,
      [
        {
          data: null,
          className: 'details-control',
          orderable: false,
          defaultContent: '',
        },
        {data: 'buyerId.name'},
        {data: 'buyerId.phoneNumber1'},
        {data: 'payment.paymentType'},
        {data: 'paid'},
        {data: 'total'},
        {data: 'createdAt'},
      ],
      'voucher',
      userId,
      null,
      [editBtn, createDeleteBtn('voucher'), createPrintBtn(), createExportBtn()]
  );

  bindDtEvents(voucherTable, 'voucher', userId);

  /*
   * Append Card View in Row
   * @param {object} d alldata
   * @param {int} count payment array length
   * @param {object} payment payment data
   *
   * @return cardview
   */
  const format = (d, count, payment) => {
    let shippingCard = '';
    let paymentCard = '';

    let details = `
    <div class="col-12">
        <table class="table table-bordered">
          <tr>
            <th>Name</th>
            <th>Quantity</th>
            <th>Selling Price</th>
          </tr>`;

    if (payment) {
      paymentCard = `
        <div class="col-md-4">
          <div class="card border-dark my-2">
            <div class="card-header">Payment</div>
            <div class="card-body text-dark">
              Bank Name: ${payment.bankName} <br>
              Account Number: ${payment.accountNumber} <br>
              Name On Card: ${payment.nameOnCard} <br>
            </div>
          </div>
        </div>`;
    }
    for (let i = 0; i < count; i++) {
      let carrier = d[i].carrier;
      let shipFrom = d[i].shipFrom;

      if (carrier === undefined && shipFrom === undefined) {
        carrier = 'None';
        shipFrom = 'None';
      }

      shippingCard += `
        <div class="col-md-4">
          <div class="card border-dark my-2">
          <div class="card-header">Shipping</div>
            <div class="card-body shipping-card-height">
              <div>
              Carrier: ${carrier} <br>
              Ship From: ${shipFrom} <br>
              </div>
            </div>
          </div>
        </div>`;
      details += ` 
      <tr>
        <td>${d[i].productName}</td>
        <td>${d[i].quantity}</td>
        <td>${d[i].sellingPrice}</td>
      </tr>  
        `;
    }
    // Name: ${d[i].productId.name} <br>
    //               Quantity: ${d[i].quantity} <br>
    //               Selling Price: ${d[i].sellingPrice} <br></br>
    return `
        <div class="row mb-2">
          ${shippingCard}
          ${paymentCard}
        </div>
        <div class="row">
              ${details}
            </table>
          </div>
        </div>
        `;
  };

  // Add event listener for opening and closing details
  $('#voucher-table tbody').on('click', 'td.details-control', function(e) {
    e.stopPropagation();

    const tr = $(this).closest('tr');
    const row = voucherTable.row(tr);
    const idx = $.inArray(tr.attr('id'), detailRows);

    if (row.child.isShown()) {
      tr.removeClass('details');
      row.child.hide();

      // Remove from the 'open' array
      detailRows.splice(idx, 1);
    } else {
      tr.addClass('details');

      const data = row.data().products;
      const count = data.length;
      const payment = row.data().payment;

      /**
       * Check if payment is bank transfer
       */
      if (payment.paymentType === 'Bank transfer') {
        row.child(format(data, count, payment)).show();
      } else {
        row.child(format(data, count, null)).show();
      }

      // Add to the 'open' array
      if (idx === -1) {
        detailRows.push(tr.attr('id'));
      }
    }
  });

  // On each draw, loop over the `detailRows` array and show any child rows
  voucherTable.on('draw', function() {
    $.each(detailRows, function(i, id) {
      $('#' + id + ' td.details-control').trigger('click');
    });
  });
});
