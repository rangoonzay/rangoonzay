$(document).ready(function() {
  /**
   * Disable going to previous page
   */
  function disablePrev() {
    window.history.forward(-3);
  }
  window.onload = disablePrev();
  window.onpageshow = function(evt) {
    if (evt.persisted) disableBack();
  };
});
