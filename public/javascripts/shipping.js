$(function() {
  const userId = JSON.parse(localStorage.getItem('user'))._id;
  const shippingTable = getDataTable(
      [
        {
          width: '25%',
          targets: [0],
        },
        {
          targets: [2],
          render: function(data) {
            if (data === '') {
              return '';
            } else {
              return moment(data)
                  .format('YYYY-MM-DD hh:mm A');
            }
          },
        },
      ],
      `/shops/api/shipping?sellerId=${userId}`,
      [
        {data: 'carrier'},
        {data: 'shipFrom'},
        {data: 'pickUpDate'},
        {data: 'shippingStatus'},
      ],
      'shipping',
      userId,
      function(row, data) {
        if (data.shippingStatus === 'Open') {
          buttonHtml('orange', 'Open', '3', row);
        } else if (data.shippingStatus === 'Shipped') {
          buttonHtml('green', 'Shipped', '3', row);
        }
      },
      null
  );

  /**
   * Add shipping status styling function
   * @param {String} color Color for shipping status
   * @param {String} status Shipping status
   * @param {String} colIndex Index of the column to be styled
   * @param {Object} row Datatable row object
   */
  const buttonHtml = (color, status, colIndex, row) => {
    $(`td:eq(${colIndex})`, row).html(`
      <button class="btn btn-sm shipping-status ${color}">
        ${status}
      </button>
    `);
    $(`td:eq(${colIndex})`, row).css('text-align', 'center');
  };

  $('#shipping-newButton').css('display', 'none');
  bindDtEvents(shippingTable, 'shipping', userId);

  $('#shipping-calendar').click(function(e) {
    e.preventDefault();
    const currentDateTime = new Date();
    $('#shipping-pickedUpBy').val(
        moment(currentDateTime).format('YYYY-MM-DD hh:mm A')
    );
  });
});
