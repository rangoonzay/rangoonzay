$(function() {
  const userId = JSON.parse(localStorage.getItem('user'))._id;

  // editBtn and its event
  const editBtn = {
    text: 'Edit',
    className: 'btn btn-outline-dark btn-sm disabled',
    attr: {id: 'product-editButton'},
    action: function(e, dt, node, config) {
      $(`#product-modal button[type=submit]`)
          .attr('id', `product-putForm`);
      $(`#product-form`).removeClass('was-validated');
      const cellIndex = dt.cell('.selected', 0).index();
      const data = dt.row(cellIndex.row).data();
      $('#product-form').attr('data-id', data._id);
      $('#product-deleteButton, #product-editButton').addClass(
          'disabled'
      );
      $(`#product-modal`).modal('show');

      for (const key in data) {
        if (key === 'inventories') {
          for (const item of data[key]) {
            const loc = item.location;

            // extract idx
            const pattern = /\d+/;
            const name = $(`#${loc}-location`).attr('name');
            const idx = parseInt(name.match(pattern)[0]);
            const nameAttr = 'inventories[' + idx + '][_id]';

            $(`#${loc}-_id`).attr('name', nameAttr);
            $(`#${loc}-_id`).val(item._id);
            $(`#${loc}-location`).val(item.location);
            $(`#${loc}-inStock`).val(item.inStock);
          }
        } else {
          $(`#product-${key}`).val(data[key]);
        }
      }
    },
  };

  const productTable = getDataTable(
      [
        {
          width: '20%',
          targets: [0],
        },
        {
          targets: 4,
          render: function(data) {
            return data.toLocaleString();
          },
        },
        {
          targets: 5,
          render: function(data) {
            return data.toLocaleString();
          },
        },
      ],
      `/shops/api/product?sellerId=${userId}`,
      [
        {data: 'name'},
        {data: 'barcode'},
        {data: 'color'},
        {data: 'brand'},
        {data: 'buyingPrice'},
        {data: 'sellingPrice'},
      ],
      'product',
      userId,
      null,
      [
        createNewBtn('product'),
        editBtn,
        createDeleteBtn('product'),
        createPrintBtn(),
        createExportBtn(),
      ]
  );

  bindDtEvents(productTable, 'product', userId);

  let selectedFiles = null;
  let trackedArray = null;

  /**
   * Image Thumbnails
   */
  $('#formControlFile').change(function(event) {
    selectedFiles = Array.prototype.slice.call(event.target.files);
    console.log(event.target.files);

    $('.hide-this').addClass('d-none');

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      // check File API supported browser
      $.each(selectedFiles, function(index, file) {
        // loop though each file
        if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
          // check supported file type
          const fRead = new FileReader(); // new filereader
          fRead.onload = (function(file) {
            // trigger function on successful read
            return function(e) {
              const thumb = `
                <div class='col-md-3 col-sm-3 thumb-img-box'>
                    <img id="thumb-img" src=${e.target.result}>
                    <span id="closeBtn${index}" class="close-thick">
                </div>`;

              // append image to output element
              $('#thumb-output').append(thumb);
            };
          })(file);
          fRead.readAsDataURL(file); // URL representing the file's data.
        }
        $(document).on('click', `#closeBtn${index}`, function(e) {
          e.preventDefault();
          $(this)
              .parent()
              .remove();

          delete selectedFiles[index];

          trackedArray = selectedFiles.filter(function() {
            return true;
          });

          if (trackedArray.length === 0) {
            $('.hide-this').removeClass('d-none');
          }
        });
      });
    } else {
      alert('Your browser doesn\'t support File API!'); // if File API is absent
    }
  });
});
