/**
 * DataTable configuration
 * @param {Array} columnDefs Column definition array
 * @param {String} ajax Data Url
 * @param {Array} columns Columns for ajax data
 * @param {String} fileName Name of the page
 * @param {String} sellerId Object ID of current user
 * @param {Callback} rowCallback function cb for table cell
 * @param {List} buttonList list of buttons to show in the page
 * @return {DataTable} datatable instance
 */
this.getDataTable = (
    columnDefs,
    ajax,
    columns,
    fileName,
    sellerId,
    rowCallback,
    buttonList
) => {
  if (buttonList === null) {
    buttonList = this.defaultButtons(fileName);
  }
  // eslint-disable-next-line new-cap
  const table = $(`#${fileName}-table`).DataTable({
    dom: 'Bfrtp',
    autoWidth: false,
    columnDefs,
    ajax,
    columns,
    rowCallback,
    buttons: {
      dom: {
        button: {
          className: '',
        },
      },
      buttons: buttonList,
    },
  });
  return table;
};

/**
 * Form submission (Delete)
 * @param {DataTable} dt DataTable instance
 * @param {String} pageName Name of the page
 */
const formDeleteEvent = (dt, pageName) => {
  $('#formDeleteButton').click(function(e) {
    e.preventDefault();

    const cellIndex = dt.cell('.selected', 0).index();
    const data = dt.row(cellIndex.row).data();
    const id = data._id;
    const url = `/shops/api/${pageName}/${id}`;

    postToServerWithHeader('delete', url, null, null, null)
        .then((result) => {
          if (result.data.error) {
            $('#failModal').modal('show');
            console.log(result.error);
          } else {
            dt.row('.selected')
                .remove()
                .draw(false);
          }
        })
        .catch((e) => {
          console.log(e);
        });

    $(`#${pageName}-deleteButton, #${pageName}-editButton`).addClass(
        'disabled'
    );
    $(`#${pageName}-newButton`).removeClass('disabled');
  });
};

/**
 * Toggle select styling on the clicked row in datatable
 * @param {Object} table Datatable object
 * @param {String} fileName fileName Name of the Page
 */
const toggleSelectEvent = (table, fileName) => {
  $(`#${fileName}-table tbody`).on('click', 'tr[role="row"]', function() {
    if ($(this).hasClass('selected')) {
      $(this).removeClass('selected');
      $(`#${fileName}-deleteButton, #${fileName}-editButton`).addClass(
          'disabled'
      );
      $(`#${fileName}-newButton`).removeClass('disabled');
    } else {
      table.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      $(`#${fileName}-deleteButton, #${fileName}-editButton`).removeClass(
          'disabled'
      );
      $(`#${fileName}-newButton`).addClass('disabled');
    }
  });
};

/**
 * Remove styling for the selected table row h/
 * @param {String} fileName Name of the page
 */
const removeSelectStylingEvent = (fileName) => {
  $('.closeButton').click(function(e) {
    e.preventDefault();
    $('.selected').removeClass('selected');
    $(`#${fileName}-deleteButton, #${fileName}-editButton`).addClass(
        'disabled'
    );
    $(`#${fileName}-newButton`).removeClass('disabled');
  });
};

/**
 * Bind events to datatable
 * @param {DataTable} dt datatable instance
 * @param {String} tableName name of the table
 * @param {String} sellerId currently logged in sellerId
 * @return {void}
 */
this.bindDtEvents = (dt, tableName, sellerId) => {
  removeSelectStylingEvent(tableName);
  toggleSelectEvent(dt, tableName);
  formCreateEvent(dt, tableName, sellerId);
  formUpdateEvent(dt, tableName);
  formDeleteEvent(dt, tableName);
};

const formCreateEvent = (dt, pageName, sellerId) => {
  const submitBtn = `#${pageName}-postForm`;
  const pageModal = `#${pageName}-modal`;
  const formTag = `#${pageName}-form`;
  const url = `/shops/api/${pageName}`;
  const validateForm = document.querySelector(formTag);

  $(document).on('click', submitBtn, function(e) {
    e.preventDefault();
    const enctype = $(formTag).attr('enctype');
    let formData;
    let headers;
    if (enctype === 'multipart/form-data') {
      formData = new FormData(validateForm);
      formData.append('sellerId', sellerId);
      headers = {
        'Content-Type': 'multipart/form-data',
      };
    } else {
      formData = extractFormFields(formTag, {sellerId});
      headers = {
        'Content-Type': 'application/json',
      };
    }

    // validate form
    if (validateForm.checkValidity() === false) {
      validateForm.classList.add('was-validated');
    } else {
      postToServerWithHeader('post', url, formData, null, headers)
          .then((result) => {
            if (result.data.error) {
              $(pageModal).modal('hide');
              $('#failModal').modal('show');
              console.log(result.data.error);
            } else {
              $(pageModal).modal('hide');
              $('#successModal').modal('show');
              $('#successModal button').attr('id', 'newSuccess');

              // Add the newly created data into table
              const res = result.data.data;
              const addRow = dt.row.add(res).draw(false);
              addRow.data()._id = res._id;

              // Add animated styling to added row
              const rowNode = addRow.node();
              $('.row-fade').removeClass();
              $(rowNode).addClass('row-fade');
            }
          })
          .catch((e) => {
            console.log(e);
          });
    }
  });
};

const formUpdateEvent = (dt, pageName) => {
  const submitBtn = `#${pageName}-putForm`;
  const formTag = `#${pageName}-form`;
  const pageModal = `#${pageName}-modal`;
  const baseURL = `/shops/api/${pageName}`;
  const validateForm = document.querySelector(formTag);

  $(document).on('click', submitBtn, function(e) {
    e.preventDefault();
    const id = $(formTag).attr('data-id');
    const url = `${baseURL}/${id}`;

    const enctype = $(formTag).attr('enctype');
    let formData;
    let headers;
    if (enctype === 'multipart/form-data') {
      formData = new FormData(validateForm);
      headers = {
        'Content-Type': 'multipart/form-data',
      };
    } else {
      formData = extractFormFields(formTag);
      headers = {
        'Content-Type': 'application/json',
      };
    }
    postToServerWithHeader('put', url, formData, null, headers)
        .then((result) => {
          if (result.data.error) {
          // Show error message modal
            $(pageModal).modal('hide');
            $('#failModal').modal('show');
            console.log(result.data.error);
          } else {
          // Show success modal
            $(pageModal).modal('hide');
            $('#successModal').modal('show');

            // Update table selected row with updated data
            const res = result.data.data;
            const row = dt.row('.selected');
            row.data(res).draw();

            // Remove styling from new button and selected row in the table
            $(`#${pageName}-newButton`).removeClass('disabled');
            $('.selected').removeClass('selected');

            // Add animated styling to updated row
            const rowNode = row.node();
            $('.row-fade').removeClass();
            $(rowNode).addClass('row-fade');
          }
        })
        .catch((e) => {
          console.log(e);
        });
  });
};

this.createNewBtn = (fileName) => {
  return {
    text: 'New',
    attr: {id: `${fileName}-newButton`},
    className: 'btn btn-outline-dark btn-sm',
    action: function(e, dt, node, config) {
      $(`#${fileName}-modal button[type=submit]`).attr(
          'id',
          `${fileName}-postForm`
      );
      $(`#${fileName}-modal`).modal('show');

      $(`#${fileName}-form`).removeClass('was-validated');
      document.getElementById(`${fileName}-form`).reset();
    },
  };
};

this.createEditBtn = (fileName) => {
  return {
    text: 'Edit',
    className: 'btn btn-outline-dark btn-sm disabled',
    attr: {id: `${fileName}-editButton`},
    action: function(e, dt, node, config) {
      $(`#${fileName}-modal button[type=submit]`).attr(
          'id',
          `${fileName}-putForm`
      );
      $(`#${fileName}-form`).removeClass('was-validated');
      $(`#${fileName}-modal`).modal('show');

      $(`#${fileName}-deleteButton, #${fileName}-editButton`).addClass(
          'disabled'
      );
      const cellIndex = dt.cell('.selected', 0).index();
      const data = dt.row(cellIndex.row).data();
      $(`#${fileName}-form`).attr('data-id', data._id);

      $.each(data, function(key, value) {
        if (value === null) {
          value = '';
        }

        $(`#${fileName}-${key}`).val(`${value}`);
      });


      // placeholder formatted datetime
      if (fileName === 'shipping') {
        const shippingDate = $(`#${fileName}-pickedUpBy`);

        data.pickedUpBy
          ? shippingDate.val(
              `${moment(data.pickedUpBy).format('YYYY-MM-DD hh:mm A')}`
          )
          : shippingDate.val('');
      }
    },
  };
};

this.createDeleteBtn = (fileName) => {
  return {
    text: 'Delete',
    className: 'btn btn-outline-dark btn-sm disabled',
    attr: {id: `${fileName}-deleteButton`},
    action: function(e, dt, node, config) {
      $('#confirmModal').modal('show');
    },
  };
};

this.createPrintBtn = () => {
  return {
    extend: 'print',
    className: 'btn btn-outline-dark btn-sm',
  };
};

this.createExportBtn = () => {
  return {
    extend: 'collection',
    background: false,
    autoClose: true,
    fade: 100,
    text: 'Export',
    className: 'btn btn-outline-dark btn-sm dropdown-toggle',
    buttons: [
      {
        extend: 'excel',
        className: 'dropdown-item',
      },
      {
        extend: 'pdf',
        className: 'dropdown-item',
      },
    ],
  };
};

this.defaultButtons = (fileName) => {
  const btnList = [];
  btnList.push(this.createNewBtn(fileName));
  btnList.push(this.createEditBtn(fileName));
  btnList.push(this.createDeleteBtn(fileName));
  btnList.push(this.createPrintBtn());
  btnList.push(this.createExportBtn());
  return btnList;
};
