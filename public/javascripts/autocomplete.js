$(function() {
  const user = JSON.parse(localStorage.getItem('user'));

  /**
   * Set up flexdatalist for product in checkout page
   */
  $('.fdl-product').flexdatalist({
    minLength: 1,
    valueProperty: 'name',
    visibleProperties: ['name', 'barcode'],
    searchIn: 'name',
    selectionRequired: true,
    url: `/shops/fdl/products?sellerId=${user._id}`,
    noResultsText: 'Add a new product',
  });

  /**
   * Set value for price when flexdatalist item is selected
   */
  $('.fdl-product').on('select:flexdatalist', function(event, set, options) {
    $('#checkout-sellingPrice').val(set.sellingPrice);
    $('#checkout-productId').val(set._id);
  });
});
