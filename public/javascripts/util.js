/**
 * Extract fields from form and combine with extra data
 * @param {String} documentId FormId to query
 * @param {Json}   extraData  Extra data to add to form fields
 *
 * @return {Json} Form fields with added fields
 */
this.extractFormFields = (documentId, extraData) => {
  const formFields = new FormData(document.querySelector(documentId));
  const formValues = {};

  for (const pair of formFields) {
    formValues[pair[0]] = pair[1];
  }
  return Object.assign(formValues, extraData);
};

/**
 * Send HTTP requests(PUT, POST, DELETE) to server
 * @param {String} method Http verbs
 * @param {String} url    URL
 * @param {Json}   data   JSON data
 * @param {Json}   params query data
 * @param {Json}   headers header data
 * @return {Json} Response from server
 */
this.postToServerWithHeader = async (method, url, data, params, headers) => {
  try {
    const response = await axios({
      method,
      url,
      data,
      params,
      headers,
    });
    return response;
  } catch (e) {
    console.log(e);
  }
};
