$(function() {
  $('#loginButton').click(async (e) => {
    e.preventDefault();

    const email = $('#InputEmail1').val();
    const password = $('#InputPassword1').val();
    const loginData = {
      email,
      password,
    };

    const response = await postToServerWithHeader(
        'post',
        '/users/login',
        loginData
    );
    console.log(response);

    if (response.data.data) {
      localStorage.setItem(
          'user',
          JSON.stringify(response.data.data.user)
      );
      localStorage.setItem('token', response.data.data.token);

      $('#InputPassword1, #InputEmail1').removeClass('is-invalid');
      // Display login success message
      $('#notification').addClass('active alert-success');
      $('#noti-text-box').addClass('success');
      $(location).attr('href', '/shops');
      /**
      setTimeout(function() {
        $(location).attr('href', '/shops');
      }, 500);
      */
    } else {
      console.log(response.data.error);
      // Display login error message
      $('#InputEmail1, #InputPassword1').val('');
      $('#InputPassword1, #InputEmail1').addClass('is-invalid');
    }
  });
});
