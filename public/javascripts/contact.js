$(function() {
  const userId = JSON.parse(localStorage.getItem('user'))._id;
  const contactTable = getDataTable(
      [
        {
          width: '25%',
          targets: [3],
        },
        {
          width: '15%',
          targets: [0, 2],
        },
      ],
      `/shops/api/contact?sellerId=${userId}`,
      [
        {data: 'name'},
        {data: 'dateOfBirth'},
        {data: 'phoneNumber1'},
        {data: 'address'},
        {data: 'city'},
        {data: 'state'},
        {data: 'facebookName'},
      ],
      'contact',
      userId,
      null,
      null
  );

  bindDtEvents(contactTable, 'contact', userId);
});
