$(function() {
  const _products = 'checkout-product';
  const _productIds = 'checkout-productIds';
  const _editVoucher = 'voucher-to-checkout';

  const user = JSON.parse(localStorage.getItem('user'));
  const voucher = JSON.parse(localStorage.getItem(_editVoucher));

  const emptyList = [];

  localStorage.setItem(_products, JSON.stringify(emptyList));

  const addProduct = (data) => {
    const prods = JSON.parse(localStorage.getItem(_products));
    prods.push(data);

    let total = 0;
    if (prods.length > 0) {
      total = prods.reduce((sub, x) => {
        return {total: sub.total + x.total};
      }).total;
    } else {
      total = data.total;
    }
    localStorage.setItem(_products, JSON.stringify(prods));

    $('#checkout-finalPrice').text(total.toLocaleString());
    $('#itemsInCart').text(prods.length.toString());
  };

  const removeProduct = (idx) => {
    const prods = JSON.parse(localStorage.getItem(_products));
    const total = prods.reduce((sub, x) => {
      return {total: sub.total + x.total};
    }).total;

    const removed = prods.splice(idx, 1)[0];
    const newTotal = parseFloat(total) - parseFloat(removed.total);

    $('#checkout-finalPrice').text(newTotal.toLocaleString());
    $('#itemsInCart').text(prods.length.toString());

    if (prods.length === 0) $('#list').addClass('d-none');
    localStorage.setItem(_products, JSON.stringify(prods));
  };

  /**
   * Add product to the cart
   */
  $('#checkout-productAdd').click(function(e) {
    e.preventDefault();

    const input = extractFormFields('#checkout-productForm');
    const quantity = parseFloat(input.quantity);
    const sellingPrice = parseFloat(input.sellingPrice);
    const total = quantity * sellingPrice;
    console.log(input);

    // Product form validation
    $('#flex0').prop('required', 'true');

    const productForm = document.querySelector('#checkout-productForm');
    if (productForm.checkValidity() === false) {
      productForm.classList.add('was-validated');
    } else {
      $('#list').removeClass('d-none');
      input.total = total;
      addProduct(input);

      addProductHTML({
        productName: input.productName,
        quantity,
        sellingPrice,
        total,
      });
      productForm.reset();
      $('#checkout-productForm').removeClass('was-validated');
    }
  });

  /**
   * When user clicks on remove icon on the added product, first we remove
   * item from the list. Then update the number of products added badge to
   * reflect the actual numbers and remove items from localStorage so that when
   * voucher is submitted, only added items are submitted.
   */
  $('body').on('click', '.remove', function() {
    const li = $(this).closest('li');
    const liIdx = li.index();
    li.remove();
    removeProduct(liIdx);
  });

  /**
   * Product add html
   * @param {Json} data JSON product meta data
   */
  const addProductHTML = (data) => {
    $('#list').append(`
      <li class="list-group-item border-0 border-show">
        <div class="row">
          <div class="col-7">
            <div class="mb-0 font">
              ${data.productName}
            </div>
          </div>
          <div class="col-4 text-right">
            ${data.total.toLocaleString()}
          </div>
          <span class="ml-2">
            <i class="fas fa-times remove"></i>
          </span>
        </div>
        <div class="row">
          <div class="col-5">
            <small class="text-muted">
              Quantity : ${data.quantity.toLocaleString()}
            </small>
          </div>
          <div class="col-7">
            <small class="d-inline-block text-muted w-100">
              Unit Price : ${data.sellingPrice.toLocaleString()}
            </small>
          </div>
        </div>
      </li>
    `);

    // Remove border of first list element
    $('#list li:eq(0)').removeClass('border-show');
  };


  $('#product-continue').click(async (e) => {
    e.preventDefault();

    const data = JSON.parse(localStorage.getItem(_products));
    const url = '/shops/api/voucheritem/b';

    if (data.length > 0) {
      const response = await postToServerWithHeader(
          'post',
          url,
          data,
      );

      const responseData = response.data.data;
      if (responseData) {
        const ids = responseData.map((x) => x._id);
        localStorage.setItem(_productIds, JSON.stringify(ids));
      }
    }
  });

  /**
   * Customer Card Add function
   */
  $('#customer-add').click(function(e) {
    e.preventDefault();

    // Customer form validation
    const customerForm = document.querySelector('#checkout-customerForm');
    if (customerForm.checkValidity() === false) {
      customerForm.classList.add('was-validated');
    } else {
      $('#customerCard').removeClass('d-none');
      $('#payment-collapse').collapse('show');
      const inputData = extractFormFields('#checkout-customerForm');

      // save data in localStorage
      localStorage.setItem('checkout-customer', JSON.stringify(inputData));

      // Add brackets and commas for visual notice
      let facebookName = inputData.facebookName;
      if (facebookName !== '') facebookName = `(${inputData.facebookName})`;

      let phoneNumber1 = inputData.phoneNumber1;
      if (inputData.phoneNumber2 !== '') {
        phoneNumber1 = `${inputData.phoneNumber1},`;
      }

      $('#customer-info').html(`
        <li id="customer-name" class="d-inline-block font mb-1">
          ${inputData.name}
        </li>
        <li id="customer-facebookName" class="d-inline-block">
          ${facebookName}
        </li>
        <li id="customer-address">
          ${inputData.address}
        </li>
        <li id="customer-city" class="d-inline-block">
          ${inputData.city},
        </li>
        <li id="customer-state" class="d-inline-block">
          ${inputData.state},
        </li>
        <li id="customer-country" class="d-inline-block">
          ${inputData.country}
        </li>
        <div>
          <li id="customer-phoneNumber1" class="d-inline-block">
            ${phoneNumber1}
          </li>
          <li id="customer-phoneNumber2" class="d-inline-block">
            ${inputData.phoneNumber2}
          </li>
        </div>
      `);

      // Hide form and remove validation
      $('#customer-collapse').collapse('hide');
      $('#checkout-customerForm').removeClass('was-validated');
    }
  });

  /**
   * Customer Card Edit Function
   */
  $('#customer-edit').click(function(e) {
    e.preventDefault();
    $('#customer-collapse').collapse('show');
    $('#customerCard').addClass('d-none');

    const id = $('#customer-id').text().trim();
    if (id) {
      $('#checkout-id').val(id);
    }

    const data = [
      'name',
      'phoneNumber1',
      'phoneNumber2',
      'address',
      'city',
      'state',
      'country',
    ];

    // Fill data in respective inputs
    for (const name of data) {
      if (name === 'city' || name === 'state' || name === 'phoneNumber1') {
        $(`#checkout-${name}`).val(
            $(`#customer-${name}`)
                .text()
                .replace(/,/g, '')
                .trim()
        );
      } else {
        $(`#checkout-${name}`).val(
            $(`#customer-${name}`)
                .text()
                .trim()
        );
      }
    }

    // Remove brackets from facebook name
    $('#checkout-facebookName').val(
        $('#customer-facebookName')
            .text()
            .replace(/[()]/g, '')
            .trim()
    );
  });

  /**
   * Collapse/show payment types/banking imagas in payment card
   * @param {String} changeSelector Select element's id
   * @param {String} classToHide Classes to hide
   */
  const togglePaymentEvent = (changeSelector, classToHide) => {
    $(changeSelector).change(function(e) {
      e.preventDefault();
      if (changeSelector === '#payment-type') {
        $('#checkout-paymentForm').removeClass('was-validated');
        $('#payment-banking').val('');
      }
      $(
          `#payment-amount,
          #bankTransfer-nameOnCard, #bankTransfer-accountNumber`
      ).val('');

      const paymentType = $('#payment-type').val();
      if (paymentType === 'Bank transfer') {
        $(
            `#payment-banking, 
            #bankTransfer-accountNumber, #bankTransfer-nameOnCard`
        ).attr('disabled', false);
      } else {
        $(
            `#payment-banking, 
          #bankTransfer-accountNumber, #bankTransfer-nameOnCard`
        ).attr('disabled', true);
      }

      const selector = $(this)
          .val()
          .replace(/\s/g, '')
          .toLowerCase();

      $(classToHide).collapse('hide');
      if (selector === 'banktransfer') {
        $(`#payment-${selector}`).collapse('show');
      }
      $(`#${selector}`).collapse('show');
    });
  };

  // Toggle payment input collapses
  togglePaymentEvent('#payment-type', '.hide-me, .image-hide');
  togglePaymentEvent('#payment-banking', '.image-hide');

  /**
   * Payment Card Add Function
   */
  $('#payment-add').click(function(e) {
    e.preventDefault();

    // Payment form validation
    const paymentForm = document.querySelector('#checkout-paymentForm');
    if (paymentForm.checkValidity() === false) {
      paymentForm.classList.add('was-validated');
    } else {
      $('#paymentCard').removeClass('d-none');
      $('#shipping-collapse').collapse('show');

      const defaultSelect = $('#payment-banking option:selected').val();
      const data = extractFormFields('#checkout-paymentForm', {
        bankName: defaultSelect,
      });

      if (!data.hasOwnProperty('accountNumber')) {
        data.accountNumber = '';
      }
      if (!data.hasOwnProperty('nameOnCard')) {
        data.nameOnCard = '';
      }

      // save values in localStorage
      localStorage.setItem('checkout-payment', JSON.stringify(data));

      // Add amount to total card
      const paidAmount = $('#payment-amount').val();

      $('.amount-text').html(`
        <div class='col-5'>Amount :</div>
        <div class='col-7'>
            <div class='float-right' id='paidAmount'>
              ${parseFloat(paidAmount).toLocaleString()}
            </div>
        </div>
      `);

      // Append data to payment card
      const type = $('#payment-type').val();

      $('#paymentType').text(type);
      if (type === 'Bank transfer') {
        $('#bankInfo').removeClass('d-none');
        $('#bank').text($('#payment-banking').val());
        $('#nameOnCard').text($('#bankTransfer-nameOnCard').val());
        $('#accountNumber').text($('#bankTransfer-accountNumber').val());
      }

      $('#payment-collapse').collapse('hide');
      $('#checkout-paymentForm').removeClass('was-validated');
    }
  });

  /**
   * Payment Card Edit Function
   */
  $('#payment-edit').click(function(e) {
    e.preventDefault();
    $('#payment-collapse').collapse('show');
    $('#payment-add').attr('disabled', false);

    // Fill data in payment card to input fields
    const type = $('#paymentType').text();

    if (type === 'Cash down') {
      $('#payment-type').val('Cash down');
    } else if (type === 'Cash on delivery') {
      $('#payment-type').val('Cash on delivery');
    } else {
      $('#payment-type').val('Bank transfer');
      $('#payment-banking').val($('#bank').text());
      $('#bankTransfer-nameOnCard').val($('#nameOnCard').text());
      $('#bankTransfer-accountNumber').val($('#accountNumber').text());
    }

    $('#bankInfo').addClass('d-none');

    const paidAmount = parseFloat(
        $('#paidAmount')
            .text()
            .replace(/,/g, '')
    );
    $('#payment-amount').val(paidAmount);

    $('#paymentCard').addClass('d-none');
    $('#checkout-submit').addClass('d-none');
  });

  /**
   * Add Shipping HTML
   * @param {JSON} data Shipping data
   */
  const addShippingHTML = (data) => {
    $('#shipping-info').html(`
      <li id="shipping-carrier">
        Carrier Name: ${data.carrier}
      </li>
      <li id="shipping-shipFrom">
        Ship From: ${data.shipFrom}
      </li>
      <li id="shipping-shippingStatus">
        Shipping Status: ${data.shippingStatus}
      </li>
    `);
  };

  /**
   * Add shipping card
   */
  $('#shipping-add').click(async function(e) {
    e.preventDefault();
    const products = JSON.parse(localStorage.getItem('checkout-product'));
    const method = $('#shippingForm').attr('method');
    const data = extractFormFields('#shippingForm',
        {products, sellerId: user._id, pickedUpBy: ''});

    let url;
    if (method === 'post') {
      delete data._id;
      url = '/shops/api/shipping';
    } else {
      const _id = $('#shipping-_id').val();
      url = `/shops/api/shipping/${_id}`;
    }

    const response = await postToServerWithHeader(
        method,
        url,
        data,
    );

    $('#shippingCard').removeClass('d-none');
    addShippingHTML(response.data.data);

    localStorage.setItem('shipping-data', JSON.stringify(response.data.data));

    $('#shipping-collapse').collapse('hide');
    $('#checkout-submit').removeClass('d-none');
    $('#shippingForm').trigger('reset');
  });

  /**
   * Shipping edit function
   */
  $('#shipping-edit').click(function(e) {
    e.preventDefault();
    $('#shippingCard').addClass('d-none');
    $('#shipping-collapse').collapse('show');
    $('#checkout-submit').addClass('d-none');
    $('#shippingForm').attr('method', 'put');

    const shippingData = JSON.parse(localStorage.getItem('shipping-data'));

    for ([key, value] of Object.entries(shippingData)) {
      $(`#shipping-${key}`).val(value);
    }
  });

  /**
   * Create pdf
   */
  $('#generateShippingLabel').click(async function(e) {
    try {
      // extract values from form and post them to backend
      const data = extractFormFields('#checkout-customerForm');
      const response = await axios({
        url: '/shops/shipping-label',
        method: 'post',
        responseType: 'blob',
        data: data,
      });
      const blob = new Blob([response.data], {
        type: 'application/pdf',
      });

      // download file
      const objectUrl = window.URL.createObjectURL(blob);
      const url = document.createElement('a');
      url.href = objectUrl;
      url.setAttribute('download', 'shipping-label.pdf');
      url.click();
    } catch (e) {
      console.log(e);
    }
  });

  /**
   * Checkout items
   */
  $('#checkout-submit').click(async function() {
    const products = JSON.parse(localStorage.getItem(_products));
    const productIds = JSON.parse(localStorage.getItem(_productIds));
    const customer = JSON.parse(localStorage.getItem('checkout-customer'));
    const payment = JSON.parse(localStorage.getItem('checkout-payment'));
    const paid = parseFloat(payment.paid);
    const total = products.reduce((sub, x) => {
      return {total: sub.total + x.total};
    }).total;

    if (voucher) {
      const data = {
        products: productIds,
        customer,
        payment,
        total,
        paid,
        sellerId: user._id,
        _id: voucher._id,
      };

      const id = voucher._id;
      const response = await postToServerWithHeader(
          'put',
          `/shops/api/voucher/${id}`,
          data
      );

      if (response.data.data) {
        $('#successModal').modal('show');
        $('#successModal button').attr('id', 'newSuccess');
        localStorage.removeItem('voucher-to-checkout');
      } else {
        $('#failModal').modal('show');
      }
    } else {
      delete payment.amount;
      delete customer._id;

      const data = {
        products: productIds,
        customer,
        payment,
        total,
        paid,
        sellerId: user._id,
      };

      const response = await postToServerWithHeader(
          'post',
          '/shops/api/voucher',
          data
      );

      if (response.data.data) {
        $('#successModal').modal('show');
        $('#successModal button').attr('id', 'newSuccess');
      } else {
        $('#failModal').modal('show');
      }

      $(`#customerCard, #paymentCard, #shippingCard, #checkout-submit, #list, 
      .amount-text`).addClass('d-none');
      $('#checkout-finalPrice').text('0');
      $('#itemsInCart').text('0');

      const node = document.getElementById('list');
      while (node.firstChild) {
        node.removeChild(node.firstChild);
      }

      document.getElementById('checkout-customerForm').reset();
      document.getElementById('checkout-paymentForm').reset();
      document.getElementById('shippingForm').reset();

      // clear items
      localStorage.setItem('checkout-product', JSON.stringify([]));
      localStorage.removeItem('checkout-customer');
      localStorage.removeItem('checkout-payment');
    }
  });

  const updateProductList = (list) => {
    $('#list').removeClass('d-none');
    let total = 0;
    for (const product of list) {
      const subtotal = product.quantity * product.sellingPrice;
      const d = product;
      d.total = subtotal;
      addProductHTML(d);
      total += subtotal;
    }
    $('#checkout-finalPrice').text(total.toLocaleString());
    $('#itemsInCart').text(list.length.toString());
    localStorage.setItem('checkout-product', JSON.stringify(list));
  };

  const updateCustomer = (customer) => {
    // Add brackets and commas for visual notice
    let facebookName = customer.facebookName;
    if (facebookName !== '') facebookName = `(${customer.facebookName})`;

    let phoneNumber1 = customer.phoneNumber1;
    if (customer.phoneNumber2 !== '') {
      phoneNumber1 = `${customer.phoneNumber1},`;
    }

    $('#customerCard').removeClass('d-none');
    $('#customer-info').html(`
      <li id="customer-id" class="font d-none mb-1">
        ${customer._id}
      </li>
      <li id="customer-name" class="d-inline-block font mb-1">
        ${customer.name}
      </li>
      <li id="customer-facebookName" class="d-inline-block">
        ${facebookName}
      </li>
      <li id="customer-address">
        ${customer.address}
      </li>
      <li id="customer-city" class="d-inline-block">
        ${customer.city},
      </li>
      <li id="customer-state" class="d-inline-block">
        ${customer.state},
      </li>
      <li id="customer-country" class="d-inline-block">
        ${customer.country}
      </li>
      <div>
        <li id="customer-phoneNumber1" class="d-inline-block">
          ${phoneNumber1}
        </li>
        <li id="customer-phoneNumber2" class="d-inline-block">
          ${customer.phoneNumber2}
        </li>
      </div>
    `);
    localStorage.setItem('checkout-customer', JSON.stringify(customer));
  };

  const updatePayment = (payment) => {
    $('#paymentCard').removeClass('d-none');

    $('.amount-text').html(`
      <div class='col-5'>Amount :</div>
      <div class='col-7'>
          <div class='float-right' id='paidAmount'>
            ${payment.paid.toLocaleString()}
          </div>
      </div>
    `);

    $('#paymentType').text(payment.paymentType);

    if (payment.paymentType === 'Bank transfer') {
      $('#bankInfo').removeClass('d-none');
      $('#bank').text(payment.bankName);
      $('#nameOnCard').text(payment.nameOnCard);
      $('#accountNumber').text(payment.accountNumber);
    }
    localStorage.setItem('checkout-payment', JSON.stringify(payment));
  };

  if (voucher) {
    updateProductList(voucher.products);
    updateCustomer(voucher.buyerId);
    updatePayment(voucher.payment);
    $('#checkout-submit').removeClass('d-none');
    localStorage.removeItem(_editVoucher);
  }
});
