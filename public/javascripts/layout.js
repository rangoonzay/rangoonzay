$(function() {
  const token = localStorage.getItem('token');
  const user = localStorage.getItem('user');

  if (user && token) {
    // Add styling to side nav links
    $(window).on('load', function() {
      $('#sidebar a').removeClass('active');
      const lastIndex = window.location.pathname
          .split('/')
          .pop();

      if (lastIndex === 'shops') {
        $('#checkoutLink').addClass('active');
      } else {
        $(`#${lastIndex}Link`).addClass('active');
      }
    });
  } else {
    $(location).attr('href', '/login');
  }

  $('#signOutButton').click((e) => {
    e.preventDefault();
    $(location).attr('href', '/login');
  });
});
