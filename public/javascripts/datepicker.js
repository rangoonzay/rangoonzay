$(function() {
  $('.input-group.date').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
    startView: 3,
    orientation: 'bottom',
  });
});
