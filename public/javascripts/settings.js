$(function() {
  // Add/remove active styles in profile navigation
  $(window).on('load', function() {
    $('#profileLink, #accountLink').removeClass('clicked, active');
    const lastIndex = window.location.pathname
        .split('/')
        .pop();
    $(`#${lastIndex}Link`).addClass('clicked');
  });
});
