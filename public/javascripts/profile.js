$(function() {
  const token = localStorage.getItem('token');
  const user = JSON.parse(localStorage.getItem('user'));
  let profilePic = null;

  /**
   * Avatar Image On change
   */
  $('#avatar').change(function(event) {
    profilePic = event.target.files[0];

    readURL(this);
    $('.filePreview').attr('display', 'block');
    $('.dropdown-menu').removeClass('show');
  });

  /**
   * Image Thumbnail
   * @param {file} input selected photo in thumbnail
   */
  readURL = (input) => {
    if (input.files && input.files[0]) {
      const reader = new FileReader();

      reader.onload = function(e) {
        $('#profilePreview').attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
    }
  };

  /**
   * Fetch and show data in profile form
   * Update Profile
   */
  $('.position-absolute').css('display', 'block');
  $.each(user, function(key, value) {
    if (value === null) {
      value = '';
    }
    if (key === 'avatar') {
      $('#profilePreview').attr('src', value);
      $('#profileImage').attr('src', value);
    } else {
      $(`#profile-${key}`).val(`${value}`);
    }
  });

  /**
   * Update Profile Button
   * First upload a photo to aws
   */
  $('#updateProfileButton').click(async (e) => {
    e.preventDefault();

    const data = new FormData();

    data.append('profileImage', profilePic);

    /**
     * Upload a profile pic to AWS
     * Use FormData and content-type form-data
     * @returns {string} Location of image in AWS
     */
    const method = 'post';
    const url = '/api/user-profile-upload';
    headers = {
      'accept': 'application/json',
      'Accept-Language': 'en-US,en;q=0.8',
      'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
    };

    const response = await postToServerWithHeader(method, url, data, headers);
    if (response) {
      // Success
      const fileName = response.data;
      avatar = fileName.location;

      // fetch data from form
      const profile = extractFormFields('#profile-form');
      profile.avatar = avatar;


      // Update Profile
      const method = 'put';
      const url = '/users/me';
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      };
      const responseData = await postToServerWithHeader(
          method,
          url,
          profile,
          headers
      );
      if (responseData) {
        if (responseData.data) {
          // Success Model Load
          $('#successModal').modal('show');
          $('#successModalMsg').html('Profile Updated Successfully');
          /**
           * Navbar Update
           */
          $('#profileImage').attr('src', responseData.data.avatar);

          // Reset localstorage User
          localStorage.setItem('user', JSON.stringify(responseData.data));
        } else {
          alert('Updated Fail');
        }
      } else {
        alert('Updated Fail');
      }
    } else {
      alert('Unable to edit Profile');
    }
  });

  /**
   * Sign Out Button
   */
  $('#signOutButton').click(async (e) => {
    e.preventDefault();
    $(location).attr('href', '/login');
  });
});
