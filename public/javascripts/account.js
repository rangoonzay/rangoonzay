$(function() {
  /** *
     * Change password
     */
  const token = localStorage.getItem('token');
  $('#changePassword').click((e) => {
    e.preventDefault();

    const oldPassword = $('#oldPassword').val();
    const newPassword = $('#newPassword').val();
    const confirmedPassword = $('#confirmedPassword').val();

    // check if new passwords are same
    if (!(newPassword === confirmedPassword)) {
      $('#alertPassword').css('display', 'inline');
      $('#alertPassword1').css('display', 'inline');
      $('#passwordForm')[0].reset();
    } else {
      data = {
        oldPassword,
        password: newPassword,
      };

      axios
          .put('/users/me/password', data, {
            headers: {
              'accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
            },
          })
          .then((response) => {
            if (response.data) {
              console.log(response.data.message);
              alert(response.data.message);
              $('#passwordForm')[0].reset();

              $('#alertPassword').css('display', 'none');
              $('#alertPassword1').css('display', 'none');
            } else {
              alert('Unable to Change password');
            }
          })
          .catch((e) => {
            alert('Your old password is wrong!');
          });
    }
  });
});
