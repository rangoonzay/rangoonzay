const jwt = require('jsonwebtoken');
const User = require('../models/user');

/**
 * Middleware for authentication header
 * @param {Request} req HTTP request
 * @param {Response} res HTTP resposne
 * @param {Middleware} next middleware to call next
 */
const auth = async (req, res, next) => {
  try {
    const token = req.cookies.token;
    const data = jwt.verify(token, process.env.SECRET_KEY);

    const user = await User.findOne({
      '_id': data._id,
      'tokens.token': token,
    });

    if (!user) {
      throw new Error();
    }

    req.token = token;
    req.user = user;

    next();
  } catch (e) {
    res.redirect('/login');
  }
};

module.exports = auth;
