# RangoonZay
A nodejs application for operation management

# Developer Guide
## Before creating MERGE REQUEST
* Remove unnecessary `console.log` statements
* Run `npx eslint <file-name>` for the files that you are gonna commit
* Run `git diff` to make sure the changes applied are what you expect

If your code doesn't comply to the above checklist, it won't be merged and you are obligated to fix them.
