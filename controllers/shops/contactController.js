const underscore = require('underscore');
const Contact = require('../../models/contact');
const mongooseCRUD = require('../../util/mongooseCRUD');
const moment = require('moment');

/**
 * List all of the contacts for sellerId
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 * @param {Callback} next middleware callback function
 */
exports.list_contact = (req, res, next) => {
  mongooseCRUD.getAllData(res, next, Contact, {sellerId: req.query.sellerId});
};

/**
 * Create contact
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 * @param {Callback} next middleware callback function
 */
exports.create_contact = (req, res, next) => {
  let dob;
  if (req.body.dateOfBirth === '') {
    dob = '';
  } else {
    dob = moment(req.body.dateOfBirth, 'DD-MM-YYYY').format('MM-DD-YYYY');
  }
  const data = underscore.extend(req.body, {
    dateOfBirth: dob,
  });
  mongooseCRUD.createData(res, next, Contact, data);
};

/**
 * Get a contact
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 * @param {Callback} next middleware callback function
 */
exports.get_contact = (req, res, next) => {
  mongooseCRUD.getSingleData(res, next, Contact, req.params.contactId);
};

/**
 * Update a contact
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 * @param {Callback} next middleware callback function
 */
exports.put_contact = (req, res, next) => {
  mongooseCRUD.updateData(res, next, Contact, req.params.contactId, req.body);
};

/**
 * Delete a contact
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 * @param {Callback} next middleware callback function
 */
exports.delete_contact = (req, res, next) => {
  mongooseCRUD.deleteData(res, next, Contact, req.params.contactId);
};
