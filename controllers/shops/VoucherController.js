const underscore = require('underscore');
const Voucher = require('../../models/voucher');
const Shipping = require('../../models/shipping');
const Contact = require('../../models/contact');
const BaseController = require('./BaseController');

/**
 * Controller Class for routing voucher http requests
 */
class VoucherController extends BaseController {
  /**
   * Constructor for VoucherController
   * @param {Model} model Voucher model instance
   */
  constructor(model) {
    super(model);

    this.list = this.list.bind(this);
    this.post = this.post.bind(this);
    this.put = this.put.bind(this);
  }

  /**
   * Returns a list of vouchers from the database
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  async list(req, res, next) {
    try {
      const data = await Voucher.find({sellerId: req.query.sellerId})
          .populate([
            {
              path: 'products',
              select: '-createdAt -updatedAt',
            },
            {path: 'buyerId', select: '-createdAt -updatedAt'},
          ])
          .exec();
      res.json({data: data, error: null});
    } catch (e) {
      console.log(e);
      res.json({data: null, error: e});
    }
  }

  /**
   * Create a voucher
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  async post(req, res, next) {
    console.log('big buck: ', req.body);
    const data = req.body;
    const sellerId = req.body.sellerId;
    const products = req.body.products;

    try {
      // Create customer if not exists
      let buyerId;
      if (data.customer.hasOwnProperty('id')) {
        buyerId = data.customer.id;
      } else {
        const contactData = underscore.extend(
            data.customer, {
              sellerId,
            });
        // eslint-disable-next-line new-cap
        const contact = await Contact(contactData).save();
        buyerId = contact._id;
      }
      console.log('Contact saved.');

      /**
       * Create voucher so that shipping can use voucherId
       */
      const voucherData = underscore.extend(
          req.body,
          {
            sellerId,
            buyerId,
          }
      );
      // eslint-disable-next-line new-cap
      const voucher = await Voucher(voucherData).save();
      console.log('Voucher saved.');

      res.json({data: voucher, error: null});
    } catch (e) {
      console.log(e);
      res.json({data: null, error: e});
    }
  }

  /**
   * Update a voucher
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  async put(req, res, next) {
    try {
      const customer = req.body.customer;
      const extendContact = underscore.extend(customer, {
        sellerId: req.body.sellerId,
      });

      const options = {
        new: true,
      };

      await Contact.findByIdAndUpdate(
          customer._id,
          extendContact,
          options
      );

      const updateVoucher = await Voucher.findByIdAndUpdate(
          req.body._id,
          req.body,
          options
      );
      res.json({data: updateVoucher, error: null});
    } catch (e) {
      console.log(e);
      res.json({data: null, error: e});
    }
  }
}

module.exports = VoucherController;
