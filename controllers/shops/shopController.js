const constants = require('../../util/constants');
const moment = require('moment');
const Voucher = require('../../models/voucher');
const mongoose = require('mongoose');

/**
 * checkout page
 * @param {Request} req Http Request Object
 * @param {Response} res Http Response Object
 */
exports.shop_home = (req, res) => {
  res.render('shops/checkout', {
    shipFrom: constants.shipFrom,
    states: constants.states,
    paymentType: constants.paymentType,
    mobileBanking: constants.mobileBanking,
    shippingStatus: constants.shippingStatus,
  });
};

/**
 * contact page
 * @param {Request} req Http Request Object
 * @param {Response} res Http Response Object
 */
exports.view_contact = (req, res) => {
  res.render('shops/contact');
};

/**
 * product page
 * @param {Request} req Http Request Object
 * @param {Response} res Http Response Object
 */
exports.view_product = (req, res) => {
  res.render('shops/product', {
    locations: constants.locations,
  });
};

const totalAgg = async (duration, sellerId, field) => {
  let startTime;
  if (duration === 'today') {
    startTime = moment().startOf('day');
  } else if (duration === 'last month') {
    startTime = moment().subtract(1, 'months');
  } else if (duration === '6 months') {
    startTime = moment().subtract(6, 'months');
  }
  const agg = await Voucher.aggregate([
    {
      $match: {
        $and: [
          {
            createdAt: {
              $gte: startTime.toDate(),
              $lte: new Date(),
            },
          },
          {
            // eslint-disable-next-line new-cap
            sellerId: mongoose.Types.ObjectId(sellerId),
          },
        ],
      },
    },
    {
      $group: {
        _id: null,
        total: {
          $sum: `$${field}`,
        },
      },
    },
  ]).exec();
  if (agg.length === 0) return 0;
  return agg[0].total;
};

/**
 * Voucher page
 * @param {Request} req Http Request Object
 * @param {Response} res Http Response Object
 */
exports.view_voucher = async (req, res) => {
  const today = await totalAgg('today', req.user._id, 'total');
  const lastMonth = await totalAgg('last month', req.user._id, 'total');
  const sixMonths = await totalAgg('6 months', req.user._id, 'total');
  const paidLastMonth = await totalAgg('last month', req.user._id, 'paid');
  res.render('shops/voucher', {
    today,
    lastMonth,
    sixMonths,
    paidLastMonth,
  });
};

/**
 * shipping page
 * @param {Request} req Http Request Object
 * @param {Response} res Http Response Object
 */
exports.view_shipping = (req, res) => {
  res.render('shops/shipping', {
    shippingStatus: constants.shippingStatus,
  });
};

/**
 * profile page
 * @param {Request} req Http Request Object
 * @param {Response} res Http Response Object
 */
exports.view_profile = (req, res) => {
  res.render('shops/settings/profile', {
    states: constants.states,
  });
};

/**
 * account page
 * @param {Request} req Http Request Object
 * @param {Response} res Http Response Object
 */
exports.view_account = (req, res) => {
  res.render('shops/settings/account');
};

