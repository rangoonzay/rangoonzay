'use strict';
const db = require('../../util/mongooseCRUD');

/**
 * Controller Class for routing http requests
 */
class BaseController {
  /**
   * Constructor for BaseController
   * @param {Model} model Mongoose model instance
   */
  constructor(model) {
    this.model = model;

    this.list = this.list.bind(this);
    this.post = this.post.bind(this);
    this.get = this.get.bind(this);
    this.put = this.put.bind(this);
    this.delete = this.delete.bind(this);
  }

  /**
   * Returns a list of items from the database
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  list(req, res, next) {
    const sellerId = req.query.sellerId;
    db.getAllData(res, next, this.model, {sellerId});
  }

  /**
   * Create an item in the database
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  post(req, res, next) {
    db.createData(res, next, this.model, req.body);
  }

  /**
   * Get an item from the database with id
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  get(req, res, next) {
    db.getSingleData(res, next, this.model, req.params.id);
  }

  /**
   * Update an item
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  put(req, res, next) {
    db.updateData(res, next, this.model, req.params.id, req.body);
  }

  /**
   * Delete an item
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  delete(req, res, next) {
    db.deleteData(res, next, this.model, req.params.id);
  }
}

module.exports = BaseController;
