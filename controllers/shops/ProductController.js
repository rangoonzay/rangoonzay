const underscore = require('underscore');
const db = require('../../util/mongooseCRUD');
const aws = require('../../util/aws');
const BaseController = require('./BaseController');

/**
 * Controller Class for routing product http requests
 */
class ProductController extends BaseController {
  /**
   * Constructor for ProductController
   * @param {Model} model Product model instance
   */
  constructor(model) {
    super(model);
    this.post = this.post.bind(this);
  }

  /**
   * Create a product
   * @param {Request} req HTTP request object
   * @param {Response} res HTTP response object
   * @param {Callback} next middleware for post processing
   */
  async post(req, res, next) {
    console.log(req.body);
    const imgUrls = await aws.upload_file(req.files);
    const data = underscore.extend(req.body, {
      imgUrls,
    });
    db.createData(res, next, this.model, data);
  }
}
module.exports = ProductController;
