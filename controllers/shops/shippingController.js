const Shipping = require('../../models/shipping');
const BaseController = require('./BaseController');

/**
 *Controller Class for routing product http requests
 */
class ShippingController extends BaseController {
  /**
   *Constructor for ShippingController
   * @param {Model} model Shipping model instance
   */
  constructor(model) {
    super(model);

    this.list = this.list.bind(this);
  }

  /**
   * List all of the shipping statuses for sellerId
   * @param {Request} req HTTP Request Object
   * @param {Response} res HTTP Response Object
   * @param {Callback} next middleware callback function
   */
  async list(req, res, next) {
    try {
      const data = await Shipping.find({
        sellerId: req.query.sellerId,
      })
          .populate([
            {path: 'products.productId', select: '-createdAt -updatedAt'},
          ])
          .exec();
      res.json({data: data, error: null});
    } catch (e) {
      console.log(e);
      res.json({data: null, error: e});
    }
  }
}

module.exports = ShippingController;
