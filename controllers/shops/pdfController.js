const pdf = require('../../util/pdfkit');
const path = require('path');
const fs = require('fs');
const uuid = require('uuid/v4');

/**
 * Generate Pdf file for shipping label
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 */
exports.createShippingLabel = async (req, res) => {
  try {
    const filename = path.join(__dirname, uuid() + '.pdf');
    await pdf(filename, req.body);
    res.sendFile(filename, (sendErr) => {
      if (!sendErr) {
        fs.unlink(filename, (err) => {
          console.log('deleted ', filename);
        });
      };
    });
  } catch (e) {
    console.log(e);
  }
};
