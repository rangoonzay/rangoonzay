const Product = require('../../models/product');

exports.flexdatalist_product = async (req, res, next) => {
  try {
    const data = await Product.find({
      sellerId: req.query.sellerId,
      name: new RegExp('^' + req.query.keyword),
    })
        .exec();
    console.log(data);
    res.json({results: data});
  } catch (e) {
    console.log(e);
    res.json({results: []});
  }
};

