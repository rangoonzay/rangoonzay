const BaseController = require('./BaseController');

/**
 * Controller Class for routing voucher http requests
 */
class VoucherItemController extends BaseController {
  /**
   * Constructor for VoucherController
   * @param {Model} model Voucher model instance
   */
  constructor(model) {
    super(model);

    this.bulkPost = this.bulkPost.bind(this);
  }

  /**
   * Create multiple voucher items
   * @param {Request} req HTTP Request Object
   * @param {Response} res HTTP Response Object
   * @param {Callback} next middleware callback function
   */
  async bulkPost(req, res, next) {
    try {
      const data = await this.model.insertMany(req.body);
      res.json({data: data, error: null});
    } catch (e) {
      console.log(e);
      res.json({data: null, error: e});
    }
  }
}

module.exports = VoucherItemController;
