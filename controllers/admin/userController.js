// TODO: DOB is needed
const User = require('../../models/user');
const bcrypt = require('bcryptjs');

/**
 * User Create
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 */
exports.user_create = async (req, res) => {
  const user = new User(req.body);
  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.send({
      data: {
        user,
        token,
      },
      error: null,
    });
  } catch (e) {
    res.status(400).send({
      data: null,
      error: e,
    });
  }
};

/**
 * User Login
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 */
exports.user_login = async (req, res) => {
  try {
    const user = await User.findByCredentials(
        req.body.email,
        req.body.password
    );
    const token = await user.generateAuthToken();
    res.cookie('token', token);
    res.send({
      data: {
        user,
        token,
      },
      error: null,
    });
  } catch (e) {
    console.log(e);
    res.send({
      data: null,
      error: 'Invalid email or password',
    });
  }
};

// Read Profile
exports.user_profile = async (req, res) => {
  res.send(req.user).status();
};

// Upate
exports.user_update = async (req, res) => {
  const updates = Object.keys(req.body);
  const fillables = [
    'name',
    'facebookName',
    'phoneNumber1',
    'phoneNumber2',
    'avatar',
    'email',
    'address',
    'city',
    'state',
  ];
  const isValidate = updates.every((update) => fillables.includes(update));

  if (!isValidate) {
    res.status(400).send({
      error: 'Invalid updates',
    });
  }

  try {
    updates.forEach((update) => (req.user[update] = req.body[update]));
    await req.user.save();
    res.send(req.user);
  } catch (e) {
    res.status(400).send(e);
  }
};

/**
 * Update User password
 * @param {Request} req HTTP Request Object
 * @param {Response} res HTTP Response Object
 */
exports.user_password_change = async (req, res) => {
  try {
    const {oldPassword, password} = req.body;
    const user = req.user;

    const isMatch = await bcrypt.compare(oldPassword, user.password);
    if (!isMatch) {
      throw new Error('Wrong old password');
    }

    console.log('still ok here');
    req.user.password = password;
    await req.user.save();

    res.send({
      message: 'Password Updated Successfully',
      error: null,
    });
  } catch (e) {
    res.status(404).send({
      data: null,
      error: 'Unable to change password',
    });
  }
};

// delete
exports.user_remove = async (req, res) => {
  try {
    await req.user.remove();
    res.send({
      info: 'successfully deleted',
    });
  } catch (e) {
    res.status(400).send(e);
  }
};
