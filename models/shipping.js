const mongoose = require('mongoose');
const moment = require('moment');
const constants = require('../util/constants');

const shippingSchema = new mongoose.Schema({
  sellerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  products: [{
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product',
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
      default: 1,
    },
  }],
  carrier: {
    type: String,
    required: true,
  },
  shipFrom: {
    type: String,
    enum: constants.locations,
  },
  shippingStatus: {
    type: String,
    enum: constants.shippingStatus,
  },
  pickedUpBy: Date,
}, {
  toObject: {virtuals: true},
  toJSON: {virtuals: true},
  timestamps: true,
});

shippingSchema.virtual('pickUpDate').get(function() {
  if (this.pickedUpBy === null) {
    return '';
  } else {
    return moment(this.pickedUpBy);
  }
});

module.exports = mongoose.model('Shipping', shippingSchema);
