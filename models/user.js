const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
// const validator = require('validator');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    unique: true,
    trim: true,
    lowercase: true,
    /**
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('Invalid Email');
      }
    },
    */
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 7,
    validate(value) {
      if (value.includes('password')) {
        throw new Error('Password cannot contain password');
      }
    },
  },
  phone: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  phoneNumber1: {
    type: String,
  },
  phoneNumber2: {
    type: String,
  },
  address: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  avatar: {
    type: String,
    default:
      'https://rangoonzay-photos.s3.ap-southeast-1.amazonaws.com/default-profile-1560490878705.jpg',
  },
  tokens: [
    {
      token: {
        type: String,
        required: true,
      },
    },
  ],
});

UserSchema.methods.generateAuthToken = async function() {
  // ho bat file ka lann khaw tae user ko use chin lo
  const user = this;

  const token = jwt.sign({_id: user._id.toString()}, process.env.SECRET_KEY, {
    expiresIn: '2 days',
  });

  user.tokens = user.tokens.concat({
    token,
  });
  await user.save();

  return token;
};

UserSchema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new Error('Unable to login');
  }

  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    throw new Error('Different Credientials');
  }
  return user;
};

UserSchema.methods.toJSON = function() {
  const user = this;
  const userObj = user.toObject();

  delete userObj.password;
  delete userObj.tokens;
  delete userObj.phone;
  return userObj;
};

UserSchema.pre('save', async function(next) {
  const user = this;

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }

  next();
});

const User = mongoose.model('users', UserSchema);
module.exports = User;
