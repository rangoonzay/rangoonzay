const mongoose = require('mongoose');
const constants = require('../util/constants');

const VoucherSchema = new mongoose.Schema({
  sellerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  buyerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Contact',
    required: true,
  },
  products: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'VoucherItem',
    required: true,
  }],
  payment: {
    paymentType: {
      type: String,
      enum: constants.paymentType,
      required: true,
    },
    bankName: String,
    nameOnCard: String,
    accountNumber: String,
  },
  paid: {
    type: Number,
    required: true,
  },
  total: {
    type: Number,
    required: true,
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('Voucher', VoucherSchema);
