const mongoose = require('mongoose');

const ContactSchema = new mongoose.Schema({
  sellerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  dateOfBirth: String,
  phoneNumber1: {
    type: String,
    required: true,
  },
  phoneNumber2: {
    type: String,
  },
  address: String,
  city: String,
  state: String,
  facebookName: String,
  facebookUrl: String,
}, {
  toObject: {virtuals: true},
  toJSON: {virtuals: true},
  timestamps: true,
});

module.exports = mongoose.model('Contact', ContactSchema);
