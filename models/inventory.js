const mongoose = require('mongoose');
const constants = require('../util/constants.js');

const InventorySchema = new mongoose.Schema({
  location: {
    type: String,
    enum: constants.locations,
    required: true,
  },
  inStock: {
    type: Number,
    required: true,
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('Inventory', InventorySchema);
