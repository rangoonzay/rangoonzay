const mongoose = require('mongoose');
const constants = require('../util/constants.js');

const ProductSchema = new mongoose.Schema({
  sellerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  name: {
    type: String,
    required: true,
  },
  brand: String,
  imgUrls: [{
    type: String,
  }],
  color: String,
  barcode: String,
  buyingPrice: {
    type: Number,
    required: true,
  },
  sellingPrice: {
    type: Number,
    required: true,
  },
  inventories: [{
    location: {
      type: String,
      enum: constants.locations,
      required: true,
    },
    inStock: {
      type: Number,
      default: 0,
      required: true,
    },
  }],
}, {
  timestamps: true,
});

module.exports = mongoose.model('Product', ProductSchema);
