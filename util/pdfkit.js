const fs = require('fs');
const PDFDocument = require('pdfkit');

const pdf = (path, customer) => {
  console.log(path, customer);
  console.log(__dirname + '/../public/images/ar-1.jpg');
  // const { name, address, city, state, phoneNumber } = customer;
  const doc = new PDFDocument({
    size: 'A5',
    margins: {
      left: 15,
      top: 15,
      bottom: 15,
      right: 15,
    },
    layout: 'portrait', // default is portrait
  });
  const writeStream = fs.createWriteStream(path);
  doc.pipe(writeStream);

  // Title for shipping label
  doc.moveTo(8, 460)
      .lineTo(150, 460)
      .stroke();
  doc.moveTo(150, 460)
      .lineTo(150, 590)
      .stroke();
  doc.moveTo(8, 590)
      .lineTo(150, 590)
      .stroke();
  doc.moveTo(8, 8)
      .lineTo(50, 8)
      .stroke();
  doc.font('Helvetica-BoldOblique', 24)
      .text('Shipping', 12, 470)
      .moveDown(0.5);
  doc.font('Helvetica-BoldOblique', 24)
      .text('Label', 12, 500)
      .moveDown(0.5);
  doc.font('Courier-Oblique', 20)
      .text('Rangoon Zay', 12, 540)
      .moveDown(0.5);

  // and some justified text wrapped into columns
  doc.font('Courier-Oblique', 12)
      .text('From', 165, 25)
      .moveDown(0.5);
  doc.font('Courier-Oblique', 12)
      .text('RangoonZay Market', 200, 45)
      .moveDown(0.5);
  doc.font('Courier-Oblique', 12)
      .text('No(123), 321st Street, Insein', 200, 60)
      .moveDown(0.5);
  doc.font('Courier-Oblique', 12)
      .text('Yangon', 200, 90)
      .moveDown(0.5);
  doc.font('Courier-Oblique', 12)
      .text('098471231', 220, 105)
      .moveDown(0.5);

  // Fit the image within the dimensions
  doc.image(__dirname + '/../public/images/ar-1.jpg', 8, 8, {
    fit: [131, 131],
  })
      .rect(8, 8, 131, 131)
      .stroke();

  // border around the frame
  doc.moveTo(8, 8)
      .lineTo(412, 8)
      .opacity(0.8)
      .stroke();
  doc.moveTo(412, 8)
      .lineTo(412, 590)
      .opacity(0.8)
      .stroke();
  doc.moveTo(8, 8)
      .lineTo(8, 590)
      .opacity(0.8)
      .stroke();
  doc.moveTo(8, 590)
      .lineTo(412, 590)
      .opacity(0.8)
      .stroke();
  doc.moveTo(8, 140)
      .lineTo(590, 140)
      .opacity(0.3)
      .stroke();

  // From
  doc.font(__dirname + '/../public/fonts/zawgyi.ttf', 17)
      .opacity(1)
      .text('To', 45, 170)
      .moveDown(0.5);
  doc.font(__dirname + '/../public/fonts/zawgyi.ttf')
      .text(customer.name, 80, 195)
      .moveDown(0.5);
  doc.font(__dirname + '/../public/fonts/zawgyi.ttf')
      .text(customer.address, 80, 214)
      .moveDown(1);
  doc.font(__dirname + '/../public/fonts/zawgyi.ttf')
      .text(customer.city + ', ' + customer.state, 80, 250)
      .moveDown(0.5);
  let phone = '';
  if (customer.phoneNumber2 === '') phone = customer.phoneNumber1;
  else phone = customer.phoneNumber1 + ', ' + customer.phoneNumber2;
  doc.font('Times-Roman')
      .text(phone, 80, 268)
      .moveDown(0.5);

  doc.moveTo(8, 310)
      .lineTo(588, 310)
      .opacity(0.3)
      .dash(5, {space: 10})
      .stroke();
  doc.font(__dirname + '/../public/fonts/barcode.ttf', 62)
      .opacity(1)
      .text('123123', 120, 360);

  // end and display the document in the iframe to the right
  doc.end();
  return new Promise((resolve, reject) => {
    writeStream.on('finish', resolve);
  });
};

module.exports = pdf;
