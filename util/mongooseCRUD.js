exports.getAllData = async (res, next, Model, queryParams, selectParams) => {
  try {
    const data = await Model.find(queryParams)
        .select(selectParams)
        .exec();
    res.json({data: data, error: null});
  } catch (e) {
    console.log(e);
    res.json({data: null, error: e});
  }
};

exports.getSingleData = async (res, next, Model, id) => {
  try {
    const data = await Model.findById(id).exec();
    res.json({data: data, error: null});
  } catch (e) {
    console.log(e);
    res.json({data: null, error: e});
  }
};

exports.createData = async (res, next, Model, data) => {
  try {
    const modelData = await new Model(data).save();
    res.json({data: modelData, error: null});
  } catch (e) {
    console.log(e);
    res.json({data: null, error: e});
  }
};

exports.updateData = async (res, next, Model, id, data) => {
  try {
    const options = {
      new: true,
    };
    const modelData = await Model
        .findByIdAndUpdate(id, data, options)
        .exec();
    res.json({data: modelData, error: null});
  } catch (e) {
    console.log(e);
    res.json({data: null, error: e});
  }
};

exports.deleteData = async (res, next, Model, id) => {
  try {
    const data = await Model.findByIdAndDelete(id);
    res.json({data: data, error: null});
  } catch (e) {
    console.log(e);
    res.json({data: null, error: e});
  }
};
