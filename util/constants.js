const constants = {
  shippingStatus: ['Open', 'Shipped'],
  transactionStatus: ['open', 'paid'],
  paymentType: ['Cash down', 'Cash on delivery', 'Bank transfer'],
  mobileBanking: ['KBZ Bank', 'AYA Bank', 'CB Bank', 'Yoma Bank'],
  itemStatus: ['Pre-order', 'In Stock'],
  itemShippingStatus: ['No shipping', 'Ship from shop', 'Ship from warehouse'],
  locations: ['Warehouse', 'Shop'],
  states: ['Ayeyarwaddy', 'Bago', 'Chin', 'Kachin', 'Kayah', 'Kayin', 'Magwe',
    'Mandalay', 'Mon', 'Rakhine', 'Sagaing', 'Shan', 'Thaninthayi', 'Yangon'],
  shipFrom: ['Warehouse', 'Shop'],
  transactionType: ['Income', 'Expense'],
  shippingStatus: ['Open', 'Shipped'],
};

module.exports = constants;
