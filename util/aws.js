const fs = require('fs');
const aws = require('aws-sdk');
const dotenv = require('dotenv');
dotenv.config();

const s3 = new aws.S3({
  apiVersion: '2006-03-01',
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

exports.upload_file = async (fileList) => {
  const metaList = [];
  for (const ufile of fileList) {
    const params = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Body: fs.createReadStream(ufile.path),
      Key: ufile.filename,
      ACL: 'public-read',
    };
    const meta = await s3.upload(params).promise();
    metaList.push(meta.Location);
  }
  return metaList;
};
