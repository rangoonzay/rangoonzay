const dotenv = require('dotenv');
const mongoose = require('mongoose');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const compression = require('compression');
const helmet = require('helmet');

const adminRouter = require('./routes/admin');
const clientRouter = require('./routes/clients');
const shopsRouter = require('./routes/shops');
const loginRouter = require('./routes/login');

const shopC = require('./controllers/shops/shopController');
const auth = require('./middleware/auth');

const app = express();

dotenv.config();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(compression());
app.use(helmet());

// parse application/json
app.use(express.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/users', adminRouter);
app.use('/clients', clientRouter);
app.use('/shops', shopsRouter);
app.use('/login', loginRouter);
app.get('/', auth, shopC.shop_home);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err);
  res.render('error');
});

// mongo connection
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  //    useFindAndModify: true,
  useFindAndModify: false,
  useCreateIndex: true,
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb connection error'));

module.exports = app;
